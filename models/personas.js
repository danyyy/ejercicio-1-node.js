const Persona = require("./persona");

class Personas {
  constructor() {
    this._listado = [];
  }

  // Método para crear persona
  crearPersona(persona = {}) {
    // Del objeto json de personas obtenemos el id y lo guardamos en el arreglo de _listado = []
    this._listado[persona.id] = persona;
  }

  // Getter para obtener el listado de personas registradas
  get listArray() {
    // aca almacenaremos cada persona
    const listado = [];
    // recorremos el objeto de json de personas
    Object.keys(this._listado).forEach(key => {
      // cada registro de persona del listado lo almacenamos en persona
      const persona = this._listado[key];
      // guardamos cada persona en listado
      listado.push(persona);
    })
    // retornamos el listado de personas
    return listado;
  }


  // Método para mostrar la lista de personas
  cargarPersonasFromArray(personas = []) {
    // Del objeto json de personas obtenemos el id y lo guardamos en el arreglo de _listado = []
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    })
  }


  // Método para eliminar una persona
  eliminarPersona(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }

}

module.exports = Personas